package com.service;

import java.util.List;

//External Service 
public interface ToDoService {
	public List<String> retrieveTodos(String user);
}
