package com.business;

import java.util.ArrayList;
import java.util.List;

import com.service.ToDoService;



public class ToDoBusinessImpl {
	private ToDoService todoService;

	ToDoBusinessImpl(ToDoService todoService) {
		this.todoService = todoService;
	}

	public List<String> retrieveTodosRelatedToSpring(String user) {
		List<String> filteredTodos = new ArrayList<String>();
		List<String> allTodos = todoService.retrieveTodos(user);
		for (String todo : allTodos) {
			if (todo.contains("Spring")) {
				filteredTodos.add(todo);
			}
		}
		return filteredTodos;
	}
}
