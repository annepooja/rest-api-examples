package com.service;

import java.util.Arrays;
import java.util.List;

//stubs are complex to implement in dynamic conditions, so using mock
public class ToDoServiceStub implements ToDoService {
	public List<String> retrieveTodos(String user) {
		return Arrays.asList("Learn Spring MVC", "Learn Spring Core",
				"Learn Junit and Mockito");
	}
}