package com.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

	public class ListTest {

		@Test
		public void TestMockListSize() {
			List list = mock(List.class);
			Mockito.when(list.size()).thenReturn(10);
			assertEquals(10, list.size());
		}

		@Test
		public void TestMockListSizeWithMultipleReturnValues() {
			List list = mock(List.class);
			Mockito.when(list.size()).thenReturn(10).thenReturn(20);
			assertEquals(10, list.size()); // First Call
			assertEquals(20, list.size()); // Second Call
		}

		@Test
		public void TestMockListGet() {
			List<String> list = mock(List.class);
			Mockito.when(list.get(0)).thenReturn("mocked successfully");
			assertEquals("mocked successfully", list.get(0));
			assertNull(list.get(1));
		}
		
		@Test
		public void TestMockListGetWithAny() {
			List<String> list = mock(List.class);
			//Argument Matchers
			Mockito.when(list.get(Mockito.anyInt())).thenReturn("success");//anyInt() is present in Mockito which is used to accept any integer 1,2,3....
			assertEquals("success", list.get(0));
			assertEquals("success", list.get(1));
		}
		
		@Test(expected = RuntimeException.class)
		public void TestMockListException()
		{
			List<String> list = mock(List.class);
			Mockito.when(list.get(Mockito.anyInt())).thenThrow(new RuntimeException("exception raised"));
			list.get(0);
		}


}
