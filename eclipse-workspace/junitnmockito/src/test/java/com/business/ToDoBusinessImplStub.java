package com.business;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.service.ToDoService;
import com.service.ToDoServiceStub;

public class ToDoBusinessImplStub {
		@Test
		public void StubTest() {
			ToDoService todoService = new ToDoServiceStub();
			ToDoBusinessImpl todoBusinessImpl = new ToDoBusinessImpl(todoService);
			List<String> todos = todoBusinessImpl.retrieveTodosRelatedToSpring("Pooja");
			assertEquals(2, todos.size());
		}
}

