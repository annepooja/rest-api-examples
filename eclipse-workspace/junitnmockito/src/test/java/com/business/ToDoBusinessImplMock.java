package com.business;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import com.service.ToDoService;

public class ToDoBusinessImplMock {
	@Test
	public void usingMockito() {
		ToDoService todoService = mock(ToDoService.class);
		List<String> allTodos = Arrays.asList("Learn Spring MVC",
				"Learn Spring", "Learn JUnit and Mockito");
		when(todoService.retrieveTodos("Pooja")).thenReturn(allTodos);
		ToDoBusinessImpl todoBusinessImpl = new ToDoBusinessImpl(todoService);
		List<String> todos = todoBusinessImpl.retrieveTodosRelatedToSpring("Pooja");
		assertEquals(2, todos.size());
	}
	
	@Test
	public void BDDMockito()
	{
		//Given
		ToDoService todoService = mock(ToDoService.class);
		List<String> allTodos = Arrays.asList("Learn Spring MVC",
				"Learn Spring", "Learn JUnit and Mockito");
		/* For BDD Mockito, instead of when-->given, thenReturn-->willReturn
		 *   given(todoService.retrieveTodos("Pooja")).willReturn(allTodos);
		 */
		given(todoService.retrieveTodos("Pooja")).willReturn(allTodos);
		ToDoBusinessImpl todoBusinessImpl = new ToDoBusinessImpl(todoService);
		
		//when
		List<String> todos = todoBusinessImpl.retrieveTodosRelatedToSpring("Pooja");
		
		//then
		/* For BDD Mockito, we have assertThat instead of assertEquals
		 *   assertThat(todos.size(), is(2));
		 */
		assertThat(todos.size(), is(2));
		
	}
}
