package com.mockito26;
 
public class StringHelper {

	public String truncateAInFirst2Positions(String str) {
		if (str.length() <= 2)
			return str.replaceAll("a", "");

		String first2Chars = str.substring(0, 2);//starts at 0 index and stops at index 2 but 2 is not included
		String stringMinusFirst2Chars = str.substring(2);

		return first2Chars.replaceAll("a", "") 
				+ stringMinusFirst2Chars;
	}

	public boolean FirstAndLastTwoCharactersTheSame(String str) {

		if (str.length() <= 1)
			return false;
		if (str.length() == 2)
			return true;

		String first2Chars = str.substring(0, 2);

		String last2Chars = str.substring(str.length() - 2);

		return first2Chars.equals(last2Chars);
	}

}
