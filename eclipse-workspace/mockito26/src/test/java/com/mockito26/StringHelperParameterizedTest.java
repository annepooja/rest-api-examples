package com.mockito26;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)//one class can have only one parameterized class, it can implement only one input(can say only one constructor)
public class StringHelperParameterizedTest {
	
	private String input;
	private String output;
	
	public StringHelperParameterizedTest(String input, String output) {
		super();
		this.input = input;
		this.output = output;
	}

	@Parameters //method should return Collections
	 public static Collection<String[]> test() {
		String expectedOutputs[][] = {{"aacd","cd"}};
		return Arrays.asList(expectedOutputs);	//converting arrays to collection
	
	}
	
	@Test
	public void truncateAInFirst2PositionsTest() {
		StringHelper helper = new StringHelper();
		assertEquals(output, helper.truncateAInFirst2Positions(input));
	}
	
}
