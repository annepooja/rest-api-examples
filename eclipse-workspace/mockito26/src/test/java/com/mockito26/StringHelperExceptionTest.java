package com.mockito26;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class StringHelperExceptionTest {

	//Test Case for NULLPOINTEREXCEPTION
			@Test(expected = NullPointerException.class)
			public void exceptionTest()
			{
				int[] num = {5,4,3,2};
				int[] expected = {2,3,4,5};
				int[] exception = null;
				Arrays.sort(num);
				Arrays.sort(exception);
				assertArrayEquals(expected, num);
			}
			
			//Test Case for Performance Test
			@Test(timeout = 1) 
			public void performanceTest()
			{
				int[] array = {36,54,23};
				for(int i=0;i>=1000000000;i++)
				{
					array[0]=i;
					Arrays.sort(array);
				}
			}
}

/* //This method will provide data to any test method that declares that its Data Provider
//is named "test1"
@DataProvider(name = "test1")
public Object[][] createData1() {
 return new Object[][] {
   { "Cedric", new Integer(36) },
   { "Anne", new Integer(37)},
 };
}

//This test method declares that its data should be supplied by the Data Provider
//named "test1"
@Test(dataProvider = "test1")
public void verifyData1(String n1, Integer n2) {
 System.out.println(n1 + " " + n2);
}
*/

