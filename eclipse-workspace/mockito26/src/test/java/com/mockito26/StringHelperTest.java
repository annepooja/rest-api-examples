package com.mockito26;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StringHelperTest {

	
	StringHelper helper;
	
	@BeforeClass //should be static
	public static void beforeClass()
	{
		System.out.println("before class execution");
	}
	
	@Before
	public void beforeTest()
	{
		helper = new StringHelper();
		System.out.println("before execution");
	}
	 
	@Test
	public void truncateAInFirst2PositionsTest() {
		assertEquals("cd", helper.truncateAInFirst2Positions("aacd"));
		System.out.println("executing truncateAInFirst2PositionsTest()");
	}
	
	@Test
	public void FirstAndLastTwoCharactersTheSameTest()
	{
		assertEquals(true, helper.FirstAndLastTwoCharactersTheSame("ab"));
		assertFalse(helper.FirstAndLastTwoCharactersTheSame("abcd"));//checks if value is equal to false
		assertTrue(helper.FirstAndLastTwoCharactersTheSame("abaab"));//checks if value is equal to true
		System.out.println("executing FirstAndLastTwoCharactersTheSameTest()");
	}
		
	@After
	public void afterTest()
	{
		System.out.println("after execution");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("after class execution");
	}
}


